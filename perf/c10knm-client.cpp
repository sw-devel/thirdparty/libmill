//
// Created by Mpho Mbotho on 2021-01-09.
//

#include <assert.h>
#include <cstring>
#include "libmill/libmill.hpp"

coroutine void sender(mevent  ev, int rounds, int& active, size_t& bytes)
{
    tcpsock s = tcpconnect(iplocal("127.0.0.1", 5555, 0), -1);
    assert(s);
    char buf[2048];
    const char *req = "HTTP/1.1 200 OK\n"
                      "Connection: Keep-Alive\n"
                      "KeepAlive: 300000\n"
                      "Strict-Transport-Security: max-age 3600; includeSubdomains\n"
                      "Content-Type: application/json\n"
                      "Server: Suil-Http-Server\n"
                      "Date: Tue, 05 Jan 2021 07:17:10 GMT\n"
                      "Content-Length: 11\n"
                      "\n"
                      "Hello World";
    int i;
    size_t nbytes;
    auto reqLen = strlen(req);
    for(i = 0; i != rounds; ++i) {
        nbytes = tcpsend(s, req, reqLen, -1);
        assert(errno == 0);
        assert(nbytes == reqLen);
        tcpflush(s, -1);
        nbytes = tcpread(s, buf, sizeof(buf), -1);
        assert(nbytes > reqLen);
        __sync_fetch_and_add(&bytes, nbytes);
    }
    tcpclose(s);
    active--;
    evnotifyone(ev);
}

int main(int argc, char *argv[])
{
    int rounds  = 1;
    int conns   = 2;
    int threads = 2;
    if (argc < 3) {
        fprintf(stderr, "Usage: ./c10knm-client <conns> <rounds> <threads>\n");
        return EXIT_FAILURE;
    }
    conns  = atoi(argv[1]);
    rounds = atoi(argv[2]);
    if (rounds == 0 or conns == 0) {
        printf("error: rounds or conns cannot be 0\n");
        return EXIT_FAILURE;
    }

    if (argc > 3) {
        threads = std::min(4, atoi(argv[3]));
    }

    auto start = mnow();
    auto lastThread = conns%threads;
    if (lastThread) threads++;
    size_t bytes{0};
    std::thread ts[threads];
    auto perThread = conns/threads;
    for (int j = 0; j < threads; j++) {
        if (lastThread and j==(threads-1))
            perThread = lastThread;
        printf("-- thread {conns %d, rounds %d}\n", perThread, rounds);
        ts[j] = masync([rounds, perThread, &bytes] {
            mevent ev = evcreate();
            int active{perThread};
            for (int i = 0; i < perThread; i++) {
                go(sender(ev, rounds, active, bytes));
            }
            while (active > 0) {
                evwait(ev, -1);
            }
        });
    }

    for (auto& t: ts) {
        if (t.joinable())
            t.join();
    }
    auto duration = mnow() - start;
    printf("conns %d, rounds/conn %d duration %0.4fs\n", conns, rounds, duration/1000.0);
    auto reqs = (conns*rounds)/(duration/1000.0);
    printf("res %d, reqs/sec %0.4f, received %0.4f Mb\n", (conns*rounds), reqs, double(bytes)/(1024*1024));
    return EXIT_SUCCESS;
}
