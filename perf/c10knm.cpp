/*

  Copyright (c) 2015 Martin Sustrik

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"),
  to deal in the Software without restriction, including without limitation
  the rights to use, copy, modify, merge, publish, distribute, sublicense,
  and/or sell copies of the Software, and to permit persons to whom
  the Software is furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included
  in all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
  IN THE SOFTWARE.

*/

#include <assert.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <sys/time.h>
#define MILL_YIELD_CLUTTERS
#include "libmill/libmill.hpp"

#include <deque>
#include <functional>
#include <cstring>

class Worker {
public:
    Worker() = default;

    void schedule(tcpsock sock)
    {
        {
            mill::Lock lk{_mtx};
            _queue.push_front(sock);
        }
        _evt.notify();
    }

    void run()
    {
        _th = masync([this] {
            go(showStats(*this));
            while (_running) {
                tcpsock sock{nullptr};
                {
                    mill::Lock lk{_mtx};
                    if (_queue.empty()) {
                        _evt.wait(lk);
                        continue;
                    }
                    sock = _queue.back();
                    _queue.pop_back();
                    _handled++;
                }
                go(handler(sock));
            }
        });
    }

    void stop()
    {
        _running = false;
        _evt.notify();
    }

    void join() {
        if (_th.joinable()) {
            _th.join();
        }
    }

    void show() {
        printf("worker-%d {handled = %zu}\n", mtid(), _handled);
        printf("---------------------------\n");
    }

    bool isRunning() { return _running; }

private:
    static coroutine void showStats(Worker& w)
    {
        while (w.isRunning()) {
            msleep(mnow()+5000);
            w.show();
        }
    }

    static coroutine void handler(tcpsock sock)
    {
        char buf[1024];
        auto offset = snprintf(buf, sizeof(buf), "%02d> ", mtid());
        while (true) {
            auto nread = tcpread(sock, &buf[offset], sizeof(buf)-offset, -1);
            if (nread <= 0)
                break;
            (void)tcpsend(sock, buf, nread+offset, -1);
            tcpflush(sock, -1);
        }
        tcpclose(sock);
    }
    std::deque<tcpsock> _queue{};
    mill::Mutex _mtx{};
    mill::Event _evt{};
    bool _running{true};
    std::thread _th;
    size_t _handled{0};
};

int main(int argc, char *argv[])
{
    auto n{2};
    Worker ws[n];
    for(auto& w: ws) {
        w.run();
    }

    srand(time(nullptr));
    tcpsock ls = tcplisten(iplocal("127.0.0.1", 5555, 0), 127);
    assert(ls);
    printf("Server listening on 127.0.0.1:5555\n");
    while (true) {
        tcpsock as = tcpaccept(ls, -1);
        if (as == nullptr) {
            printf("accept failed: %s\n", strerror(errno));
            break;
        }
        auto worker = std::rand()%n;
        ws[worker].schedule(as);
    }

    for (auto& w: ws) {
        w.stop();
        w.join();
    }

    return EXIT_SUCCESS;
}


