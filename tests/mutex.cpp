//
// Created by Mpho Mbotho on 2021-01-08.
//
#define MILL_YIELD_CLUTTERS
#include "libmill/libmill.h"
#include "debug.h"
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

#include <thread>
#include <vector>

coroutine void produce(mmutex mtx, int& var) {
    int sleep = rand() % 10 + 1;
    msleep(mnow() + sleep);
    mtxacquire(mtx);
    var++;
    sleep = rand() % 40 + 1;
    msleep(mnow() + sleep);
    mtxrelease(mtx);
}

coroutine void eventNotify(mevent evt, int64_t wait)
{
    if (wait > 0)
        msleep(mnow()+wait);
    evnotify(evt);
}

int main(int argc, char *argv[])
{
    int n = 100, t = int(std::thread::hardware_concurrency() - 1);
    if (t == 0) {
        // test not supported
        return EXIT_SUCCESS;

    }

    srand (time(nullptr));
    mmutex mtx = mtxcreate(0);
    int prod = 0, expect = (3*t)+(n*t);
    auto producer = [n, mtx, &prod] {
        for (int i = 0; i < n; i++) {
            go(produce(mtx, prod));
        }
        produce(mtx, prod);
        produce(mtx, prod);
        produce(mtx, prod);
    };

    std::thread ts[t];
    for (int i = 0 ; i < t; i++) {
        ts[i] = std::thread([&producer] {
            producer();
        });
    }

    mevent evt = evcreate();
    go(eventNotify(evt, 500));
    auto s = evwait(evt, -1);
    assert(s == 0);
    go(eventNotify(evt, 1000));
    s = evwait(evt, mnow() + 500);
    assert(s == -1);

    for (auto& t : ts) {
        if (t.joinable())
            t.join();
    }
    assert(prod == expect);
    return 0;
}