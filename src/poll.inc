/*

  Copyright (c) 2015 Martin Sustrik

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"),
  to deal in the Software without restriction, including without limitation
  the rights to use, copy, modify, merge, publish, distribute, sublicense,
  and/or sell copies of the Software, and to permit persons to whom
  the Software is furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included
  in all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
  IN THE SOFTWARE.

*/

#include <errno.h>
#include <poll.h>
#include <stddef.h>
#include <stdlib.h>
#include <unistd.h>

#include "cr.h"
#include "list.h"
#include "utils.h"
#include "scope.h"


/* The item at a specific index in this array corresponds to the entry
   in mill_pollset fds with the same index. */
struct mill_pollset_item {
    struct mill_cr *in;
    struct mill_cr *out;
};

/* Find pollset index by fd. If fd is not in pollset, return the index after
   the last item. TODO: This is O(n) operation! */
static int mill_find_pollset(int fd) {
    int i;
    for(i = 0; i != mgs(poller).pollset_size; ++i) {
        if(mgs(poller).pollset_fds[i].fd == fd)
            break;
    }
    return i;
}

void mill_poller_enter(struct mill_poller *poller)
{

}

void mill_poller_init(void) {
    errno = 0;
}

void mill_poller_postfork(void) {
    mgs(poller).pollset_size = 0;
    mgs(poller).pollset_capacity = 0;
    mgs(poller).pollset_fds = NULL;
    mgs(poller).pollset_items = NULL;
}

static void mill_poller_add(int fd, int events) {
    int i = mill_find_pollset(fd);
    /* Grow the pollset as needed. */
    if(i == mgs(poller).pollset_size) {
        if(mgs(poller).pollset_size == mgs(poller).pollset_capacity) {
            mgs(poller).pollset_capacity = mgs(poller).pollset_capacity ?
                mgs(poller).pollset_capacity * 2 : 64;
            mgs(poller).pollset_fds = realloc(mgs(poller).pollset_fds,
                mgs(poller).pollset_capacity * sizeof(struct pollfd));
            mgs(poller).pollset_items = realloc(mgs(poller).pollset_items,
                mgs(poller).pollset_capacity * sizeof(struct mill_pollset_item));
        }
        ++mgs(poller).pollset_size;
        mgs(poller).pollset_fds[i].fd = fd;
        mgs(poller).pollset_fds[i].events = 0;
        mgs(poller).pollset_fds[i].revents = 0;
        mgs(poller).pollset_items[i].in = NULL;
        mgs(poller).pollset_items[i].out = NULL;
    }
    /* Register the new file descriptor in the pollset. */
    if(events & FDW_IN) {
        if(mill_slow(mgs(poller).pollset_items[i].in))
            mill_panic(
                "multiple coroutines waiting for a single file descriptor");
        mgs(poller).pollset_fds[i].events |= POLLIN;
        mgs(poller).pollset_items[i].in = mgs(cr).running;
    }
    if(events & FDW_OUT) {
        if(mill_slow(mgs(poller).pollset_items[i].out))
            mill_panic(
                "multiple coroutines waiting for a single file descriptor");
        mgs(poller).pollset_fds[i].events |= POLLOUT;
        mgs(poller).pollset_items[i].out = mgs(cr).running;
    }
}

static void mill_poller_rm(struct mill_cr *cr) {
    mill_assert(cr->fd != -1);
    int i = mill_find_pollset(cr->fd);
    mill_assert(i < mgs(poller).pollset_size);
    if(mgs(poller).pollset_items[i].in == cr) {
        mgs(poller).pollset_items[i].in = NULL;
        mgs(poller).pollset_fds[i].events &= ~POLLIN;
        cr->fd = -1;
    }
    if(mgs(poller).pollset_items[i].out == cr) {
        mgs(poller).pollset_items[i].out = NULL;
        mgs(poller).pollset_fds[i].events &= ~POLLOUT;
        cr->fd = -1;
    }
    if(!mgs(poller).pollset_fds[i].events) {
        --mgs(poller).pollset_size;
        if(i < mgs(poller).pollset_size) {
            mgs(poller).pollset_items[i] = mgs(poller).pollset_items[mgs(poller).pollset_size];
            mgs(poller).pollset_fds[i] = mgs(poller).pollset_fds[mgs(poller).pollset_size];
        }
    }
}

static void mill_poller_clean(int fd) {
}

static int mill_poller_wait(int timeout) {
    /* Wait for events. */
    int numevs;
    while(1) {
        numevs = poll(mgs(poller).pollset_fds, mgs(poller).pollset_size, timeout);
        if(numevs < 0 && errno == EINTR)
            continue;
        mill_assert(numevs >= 0);
        break;  
    }
    /* Fire file descriptor events. */
    int result = numevs > 0 ? 1 : 0;
    int i;
    for(i = 0; i < mgs(poller).pollset_size && numevs; ++i) {
        int inevents = 0;
        int outevents = 0;
        if (!mgs(poller).pollset_fds[i].revents)
            continue;
        /* Set the result values. */
        if(mgs(poller).pollset_fds[i].revents & POLLIN)
            inevents |= FDW_IN;
        if(mgs(poller).pollset_fds[i].revents & POLLOUT)
            outevents |= FDW_OUT;
        if(mgs(poller).pollset_fds[i].revents & (POLLERR | POLLHUP | POLLNVAL)) {
            inevents |= FDW_ERR;
            outevents |= FDW_ERR;
        }
        mgs(poller).pollset_fds[i].revents = 0;
        /* Resume the blocked coroutines. */
        if(mgs(poller).pollset_items[i].in &&
              mgs(poller).pollset_items[i].in == mgs(poller).pollset_items[i].out) {
            struct mill_cr *cr = mgs(poller).pollset_items[i].in;
            cr->fd = -1;
            mill_resume(cr, inevents | outevents);
            mgs(poller).pollset_fds[i].events = 0;
            mgs(poller).pollset_items[i].in = NULL;
            mgs(poller).pollset_items[i].out = NULL;
            if(mill_timer_enabled(&cr->timer))
                mill_timer_rm(&cr->timer);
        }
        else {
            if(mgs(poller).pollset_items[i].in && inevents) {
                struct mill_cr *cr = mgs(poller).pollset_items[i].in;
                cr->fd = -1;
                mill_resume(cr, inevents);
                mgs(poller).pollset_fds[i].events &= ~POLLIN;
                mgs(poller).pollset_items[i].in = NULL;
                if(mill_timer_enabled(&cr->timer))
                    mill_timer_rm(&cr->timer);
            }
            if(mgs(poller).pollset_items[i].out && outevents) {
                struct mill_cr *cr = mgs(poller).pollset_items[i].out;
                cr->fd = -1;
                mill_resume(cr, outevents);
                mgs(poller).pollset_fds[i].events &= ~POLLOUT;
                mgs(poller).pollset_items[i].out = NULL;
                if(mill_timer_enabled(&cr->timer))
                    mill_timer_rm(&cr->timer);
            }
        }
        /* If nobody is polling for the fd remove it from the pollset. */
        if(!mgs(poller).pollset_fds[i].events) {
            mill_assert(!mgs(poller).pollset_items[i].in &&
                !mgs(poller).pollset_items[i].out);
            --mgs(poller).pollset_size;
            if(i < mgs(poller).pollset_size) {
                mgs(poller).pollset_fds[i] = mgs(poller).pollset_fds[mgs(poller).pollset_size];
                mgs(poller).pollset_items[i] = mgs(poller).pollset_items[mgs(poller).pollset_size];
            }
            --i;
        }
        --numevs;
    }
    return result;
}

