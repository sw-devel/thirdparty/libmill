/*

  Copyright (c) 2015 Martin Sustrik

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"),
  to deal in the Software without restriction, including without limitation
  the rights to use, copy, modify, merge, publish, distribute, sublicense,
  and/or sell copies of the Software, and to permit persons to whom
  the Software is furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included
  in all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
  IN THE SOFTWARE.

*/

#include <errno.h>
#include <limits.h>
#include <stdint.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/event.h>
#include <sys/resource.h>
#include <sys/time.h>

#include "cr.h"
#include "utils.h"
#include "scope.h"

#define MILL_ENDLIST 0xffffffff

#define MILL_CHNGSSIZE 128
#define MILL_EVSSIZE 128

struct mill_crpair {
    struct mill_cr *in;
    struct mill_cr *out;
    uint16_t currevs;
    uint32_t abortevs;
    uint16_t firing;
    /* 1-based index, 0 stands for "not part of the list", MILL_ENDLIST
       stads for "no more elements in the list. */
    uint32_t next;
};

void mill_poller_enter(struct mill_poller *poller)
{

}

void mill_poller_init(void) {
    struct rlimit rlim;
    int rc = getrlimit(RLIMIT_NOFILE, &rlim);
    if(mill_slow(rc < 0)) return;
    mgs(poller).ncrpairs = rlim.rlim_max;
    /* The above behaves weirdly on newer versions of OSX, ruturning limit
       of -1. Fix it by using OPEN_MAX instead. */
    if(mgs(poller).ncrpairs < 0)
        mgs(poller).ncrpairs = OPEN_MAX;
    mgs(poller).crpairs = (struct mill_crpair*)
        calloc(mgs(poller).ncrpairs, sizeof(struct mill_crpair));
    if(mill_slow(!mgs(poller).crpairs)) {errno = ENOMEM; return;}
    mgs(poller).kfd = kqueue();
    if(mill_slow(mgs(poller).kfd < 0)) {
        free(mgs(poller).crpairs);
        mgs(poller).crpairs = NULL;
        return;
    }
    errno = 0;
}

void mill_poller_postfork(void) {
    if(mgs(poller).kfd != -1) {
        /* TODO: kqueue documentation says that a kqueue descriptor won't
           survive a fork. However, implementations seem to diverge.
           On FreeBSD the following function succeeds. On OSX it returns
           EACCESS. Therefore we ignore the return value. */
        close(mgs(poller).kfd);
    }
    mgs(poller).kfd = -1;
    mgs(poller).crpairs = NULL;
    mgs(poller).ncrpairs = 0;
    mgs(poller).changelist = MILL_ENDLIST;
    mill_poller_init();
}

static void mill_poller_add(int fd, int events) {
    struct mill_crpair *crp = &mgs(poller).crpairs[fd];
    if(events & FDW_IN) {
        if(crp->in)
            mill_panic(
                "multiple coroutines waiting for a single file descriptor");
        crp->in = mgs(cr).running;
    }
    if(events & FDW_OUT) {
        if(crp->out)
            mill_panic(
                "multiple coroutines waiting for a single file descriptor");
        crp->out = mgs(cr).running;
    }
    if(!crp->next) {
        crp->next = mgs(poller).changelist;
        mgs(poller).changelist = fd + 1;
    }
}

static void mill_poller_rm(struct mill_cr *cr) {
    int fd = cr->fd;
    mill_assert(fd != -1);
    struct mill_crpair *crp = &mgs(poller).crpairs[fd];
    if(crp->in == cr) {
        crp->in = NULL;
        cr->fd = -1;
    }
    if(crp->out == cr) {
        crp->out = NULL;
        cr->fd = -1;
    }
    if(!crp->next) {
        crp->next = mgs(poller).changelist;
        mgs(poller).changelist = fd + 1;
    }
}

static void mill_poller_clean(int fd) {
    struct mill_crpair *crp = &mgs(poller).crpairs[fd];
    mill_assert(!crp->in);
    mill_assert(!crp->out);
    /* Remove the file descriptor from the pollset, if it is still there. */
    int nevs = 0;
    struct kevent evs[2];
    if(crp->currevs & FDW_IN) {
        EV_SET(&evs[nevs], fd, EVFILT_READ, EV_DELETE, 0, 0, 0);
        ++nevs;
    }
    if(crp->currevs & FDW_OUT) {
        EV_SET(&evs[nevs], fd, EVFILT_WRITE, EV_DELETE, 0, 0, 0);
        ++nevs;
    }
    if(nevs) {
        int rc = kevent(mgs(poller).kfd, evs, nevs, NULL, 0, NULL);
        mill_assert(rc != -1);
    }
    /* Clean up the cache. */
    crp->currevs = 0;
    if(!crp->next) {
        crp->next = mgs(poller).changelist;
        mgs(poller).changelist = fd + 1;
    }
}

void mill_poller_clear(int fd) {
    struct mill_crpair *crp = &mgs(poller).crpairs[fd];
    crp->abortevs = 0;
    int nevs = 0;
    struct kevent evs[2];
    if (crp->in) {
        crp->abortevs |= FDW_IN;
        EV_SET(&evs[nevs], fd, EVFILT_READ, EV_DELETE, 0, 0, 0);
        ++nevs;
    }
    if (crp->out) {
        crp->abortevs |= FDW_OUT;
        EV_SET(&evs[nevs], fd, EVFILT_WRITE, EV_DELETE, 0, 0, 0);
        ++nevs;
    }

    /* Remove the file descriptor from the pollset, if it is still present. */
    if(nevs) {
        int rc = kevent(mgs(poller).kfd, evs, nevs, NULL, 0, NULL);
        mill_assert(rc != -1);
        crp->currevs = 0;
    }

    /* Clean the cache. */
    if(!crp->next) {
        crp->next = mgs(poller).changelist;
        mgs(poller).changelist = fd + 1;
    }

    if (crp->abortevs&FDW_IN) {
        struct mill_cr *cr = crp->in;
        crp->abortevs &= ~FDW_IN;
        mill_resume(cr, FDW_ERR);
        mill_poller_rm(cr);
        if(mill_timer_enabled(&cr->timer))
            mill_timer_rm(&cr->timer);
    }
    if (crp->abortevs&FDW_OUT) {
        struct mill_cr *cr = crp->out;
        crp->abortevs &= ~FDW_OUT;
        mill_resume(cr, FDW_ERR);
        mill_poller_rm(cr);
        if(mill_timer_enabled(&cr->timer))
            mill_timer_rm(&cr->timer);
    }
}

static int mill_poller_wait(int timeout) {
    /* Apply any changes to the pollset. */
    struct kevent chngs[MILL_CHNGSSIZE];
    int nchngs = 0;
    while(mgs(poller).changelist != MILL_ENDLIST) {
        /* Flush the changes to the pollset even if there is one emtpy entry
           left in the changeset. That way we make sure that both in & out
           associated with the next file descriptor can be filled in if we
           choose not to flush the changes yet. */
        if(nchngs >= MILL_CHNGSSIZE - 1) {
            int rc = kevent(mgs(poller).kfd, chngs, nchngs, NULL, 0, NULL);
            mill_assert(rc != -1);
            nchngs = 0;
        }
        int fd = mgs(poller).changelist - 1;
        struct mill_crpair *crp = &mgs(poller).crpairs[fd];
        if(crp->in) {
            if(!(crp->currevs & FDW_IN)) {
                EV_SET(&chngs[nchngs], fd, EVFILT_READ, EV_ADD, 0, 0, 0);
                crp->currevs |= FDW_IN;
                ++nchngs;
            }
        }
        else {
            if(crp->currevs & FDW_IN) {
                EV_SET(&chngs[nchngs], fd, EVFILT_READ, EV_DELETE, 0, 0, 0);
                crp->currevs &= ~FDW_IN;
                ++nchngs;
            }
        }
        if(crp->out) {
            if(!(crp->currevs & FDW_OUT)) {
                EV_SET(&chngs[nchngs], fd, EVFILT_WRITE, EV_ADD, 0, 0, 0);
                crp->currevs |= FDW_OUT;
                ++nchngs;
           }
        }
        else {
            if(crp->currevs & FDW_OUT) {
                EV_SET(&chngs[nchngs], fd, EVFILT_WRITE, EV_DELETE, 0, 0, 0);
                crp->currevs &= ~FDW_OUT;
                ++nchngs;
            }
        }
        crp->firing = 0;
        mgs(poller).changelist = crp->next;
        crp->next = 0;
    }
    /* Wait for events. */
    struct kevent evs[MILL_EVSSIZE];
    int nevs;
    while(1) {
        struct timespec ts;
        if(timeout >= 0) {
            ts.tv_sec = timeout / 1000;
            ts.tv_nsec = (((long)timeout) % 1000) * 1000000;
        }
        nevs = kevent(mgs(poller).kfd, chngs, nchngs, evs, MILL_EVSSIZE,
            timeout < 0 ? NULL : &ts);
        if(nevs < 0 && errno == EINTR)
            continue;
        mill_assert(nevs >= 0);
        break;
    }
    /* Join events on file descriptor basis. */
    int i;
    for(i = 0; i != nevs; ++i) {
        mill_assert(evs[i].flags != EV_ERROR);
        int fd = (int)evs[i].ident;
        struct mill_crpair *crp = &mgs(poller).crpairs[fd];
        /* Add firing event to the result list. */
        if(evs[i].flags == EV_EOF)
            crp->firing |= FDW_ERR;
        else {
            if(evs[i].filter == EVFILT_READ)
                crp->firing |= FDW_IN;
            if(evs[i].filter == EVFILT_WRITE)
                crp->firing |= FDW_OUT;
        }
        if(!crp->next) {
            crp->next = mgs(poller).changelist;
            mgs(poller).changelist = fd + 1;
        }
    }
    /* Resume the blocked coroutines. */
    uint32_t chl = mgs(poller).changelist;
    while(chl != MILL_ENDLIST) {
        int fd = chl - 1;
        struct mill_crpair *crp = &mgs(poller).crpairs[fd];
        if(crp->in == crp->out) {
            struct mill_cr *cr = crp->in;
            mill_assert(crp->in);
            mill_resume(cr, crp->firing);
            cr->fd = -1;
            crp->in = NULL;
            crp->out = NULL;
            if(mill_timer_enabled(&cr->timer))
                mill_timer_rm(&cr->timer);
        }
        else {
            if(crp->in) {
                struct mill_cr *cr = crp->in;
                mill_resume(cr, crp->firing & (FDW_IN | FDW_ERR));
                cr->fd = -1;
                crp->in = NULL;
                if(mill_timer_enabled(&cr->timer))
                    mill_timer_rm(&cr->timer);
            }
            if(crp->out) {
                struct mill_cr *cr = crp->out;
                mill_resume(cr, crp->firing & (FDW_OUT | FDW_ERR));
                cr->fd = -1;
                crp->out = NULL;
                if(mill_timer_enabled(&cr->timer))
                    mill_timer_rm(&cr->timer);
            }
        }
        crp->firing = 0;
        chl = crp->next;
    }    
    /* Return 0 in case of time out. 1 if at least one coroutine was resumed. */
    return nevs > 0 ? 1 : 0;
}

