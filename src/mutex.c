//
// Created by Mpho Mbotho on 2021-01-08.
//
#include "mutex.h"
#include "libmill/libmill.h"

extern void mill_poller_poke(struct mill_scope* scope);

static void mill_event_callback(struct mill_timer *timer)
{
    mill_assert(timer != NULL);
    struct mill_cr *cr = mill_cont(timer, struct mill_cr, timer);
    struct mill_event *evt = (struct mill_event*) timer->data;
    timer->data = NULL;
    mill_lock_acquire(&evt->lk);
    if (!mill_list_empty(&evt->waiting)) {
        mill_list_erase(&evt->waiting, &cr->list);
        mill_lock_release(&evt->lk);
        mill_assert(cr->state == MILL_BLOCKED);
        mill_wakeup(cr->scope, cr, -1);
        mill_poller_poke(cr->scope);
    }
}

struct mill_mutex* mill_mutex_create_(int is, const char* current)
{
    struct mill_mutex *mtx = NULL;
    mtx = malloc(sizeof(struct mill_mutex));
    if (mtx == NULL) {
        return NULL;
    }

    (void) is;
    mtx->acquired = 0;
    mtx->id = current;
    mill_lock_init(&mtx->lk);
    mill_slist_init(&mtx->waiting);
    return mtx;
}

void mill_mutex_acquire_(struct mill_mutex* mtx, const char* current) {
    mill_assert(mtx != NULL);
    mill_lock_acquire(&mtx->lk);
    if (mtx->acquired != 0) {
        mill_slist_push_back(&mtx->waiting, &mgs(cr).running->ready);
        mill_lock_release(&mtx->lk);

        mgs(cr).running->state = MILL_BLOCKED;
        mgs(cr).running->is_ready = 0;
        mill_set_current(&mgs(cr).running->debug, current);
        mill_suspend();
    }
    else {
        mtx->acquired = 1;
        mill_lock_release(&mtx->lk);
    }
}

void mill_mutex_release_(struct mill_mutex* mtx)
{
    mill_assert(mtx != NULL);
    mill_lock_acquire(&mtx->lk);
    if (!mill_slist_empty(&mtx->waiting)) {
        // wake up single coroutine
        struct mill_slist_item *it = mill_slist_pop(&mtx->waiting);

        struct mill_cr *next = mill_cont(it, struct mill_cr, ready);
        mill_assert(next->state == MILL_BLOCKED);
        mill_trace(MILL_HERE_, "mutex release %d/%d\n", next->scope->tid, next->debug.id);
        mill_wakeup(next->scope, next, 0);
        mill_poller_poke(next->scope);
    }
    else {
        mtx->acquired = 0;
    }
    mill_lock_release(&mtx->lk);
}

int  mill_mutex_try_acquire_(struct mill_mutex* mtx)
{
    mill_assert(mtx != NULL);
    mill_lock_acquire(&mtx->lk);
    if (!mtx->acquired) {
        mtx->acquired = 1;
        mill_lock_release(&mtx->lk);
        return 1;
    }
    mill_lock_release(&mtx->lk);
    return 0;
}

void mill_mutex_destroy_(struct mill_mutex *mtx)
{
    if (mtx == NULL) {
        return;
    }
    mill_lock_acquire(&mtx->lk);
    mill_assert(mill_slist_empty(&mtx->waiting));
    mill_lock_release(&mtx->lk);
    mill_lock_destroy(&mtx->lk);
    free(mtx);
}


struct mill_event* mill_event_create_(const char* tag)
{
    struct mill_event* evt = calloc(sizeof(struct  mill_event), 1);
    mill_assert(evt != NULL);
    evt->id = tag;
    evt->signaled = 0;
    mill_list_init(&evt->waiting);
    mill_lock_init(&evt->lk);
    return evt;
}

int  mill_event_wait_(struct mill_event* evt, int64_t timeout, const char *current)
{
    mill_assert(evt != NULL);
    mill_lock_acquire(&evt->lk);
    mill_list_insert(&evt->waiting, &mgs(cr).running->list, evt->waiting.last);
    mill_lock_release(&evt->lk);
    mgs(cr).running->state = MILL_BLOCKED;
    mgs(cr).running->is_ready = 0;
    mill_set_current(&mgs(cr).running->debug, current);
    if (timeout > 0) {
        mgs(cr).running->timer.data = evt;
        mill_timer_add(&mgs(cr).running->timer, timeout, mill_event_callback);
    }

    return mill_suspend();
}

static void mill_event_notify_generic(struct mill_event* evt, int is_one)
{
    mill_lock_acquire(&evt->lk);
    while (!mill_list_empty(&evt->waiting)) {
        // wake up al coroutines
        struct mill_list_item* it = mill_list_begin(&evt->waiting);
        struct mill_cr* next = mill_cont(it, struct mill_cr, list);
        mill_list_erase(&evt->waiting, it);
        mill_lock_release(&evt->lk);

        mill_assert(next->state == MILL_BLOCKED);
        if(mill_timer_enabled(&next->timer)) {
            mill_timer_rm(&next->timer);
        }
        mill_wakeup(next->scope, next, 0);
        mill_poller_poke(next->scope);
        if (is_one)
            break;

        mill_lock_acquire(&evt->lk);
    }
    mill_lock_release(&evt->lk);
}

inline void mill_event_notify_(struct mill_event* evt)
{
    mill_event_notify_generic(evt, 0);
}

inline void mill_event_notify_one_(struct mill_event* evt)
{
    mill_event_notify_generic(evt, 1);
}

void mill_event_destroy_(struct mill_event* evt)
{
    if (evt == NULL) {
        return;
    }
    mill_lock_acquire(&evt->lk);
    mill_assert(mill_slist_empty(&evt->waiting));
    mill_lock_release(&evt->lk);
    mill_lock_destroy(&evt->lk);
    free(evt);
}