//
// Created by Mpho Mbotho on 2020-09-25.
//

#include "compat.h"

#ifdef __APPLE__

ssize_t sendfile_compat(int out_fd, int in_fd, off_t *offset, size_t count)
{
    off_t len = count;
    int status;
    if ((status = sendfile(in_fd, out_fd, *offset, &len, 0, 0)) == 0) {
        *offset += len;
        status = len;
    }
    return  status;
}

#endif