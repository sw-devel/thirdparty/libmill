//
// Created by Mpho Mbotho on 2021-01-08.
//

#include "scope.h"
#include "libmill/libmill.h"

#include <unistd.h>
#include <fcntl.h>
#include <pthread.h>

__thread struct mill_scope g_mill_scope = {
        .cr = {
            .valbuf_size = 124,
            .main_valbuf = {0},
            .main = {0},
            .running = NULL,
            .ready = {0},
            .woke  = {0}
        },
        .dbg = {
            .next_cr_id = 1,
            .all_crs = {0},
            .next_chan_id = 1,
            .all_chans = {0},
        },
        .poller = {
            .initialised = 0
        },
        .ip = {
            .dns_conf = NULL,
            .dns_hints = NULL,
            .dns_hosts = NULL
        }
#if defined(SSL_SUPPORTED)
        , .ssl = {
            .cli_ctx = NULL
        }
#endif
        , .stack = {
            .stack_size = 256 * 1024 - 256,
            .sanitised_stack_size = 0,
            .max_cached_stacks = 64,
            .num_cached_stacks = 0,
            .cached_stacks = {0},
            .boundary = 512
        },
        .poker = {
            .fd = {-1, -1},
            .on = 0,
            .waiting = 0
        },
        .timers = {0},
        .chan_choose_seqnum = 0,
        .entered = 0,
        .tid = 0
};

extern void mill_poller_enter(struct mill_poller *poller);

uint32_t mill_tid_(void)
{
    return g_mill_scope.tid;
}

static int s_tid = 0;
static long int CPU_CORE_COUNT = 0;
__attribute__ ((constructor)) void mill_scope_enter_()
{
    if (CPU_CORE_COUNT == 0)
        CPU_CORE_COUNT = sysconf(_SC_NPROCESSORS_ONLN);

    if (mill_slow(g_mill_scope.entered == 0)) {
        mgs(tid) = __sync_fetch_and_add(&s_tid, 1);
#if defined(MILL_LIMIT_THREADS)
        // spinlock might lead to performance hit if we exceed processor parallelism
        mill_assert(mgs(tid) < CPU_CORE_COUNT);
#endif

        g_mill_scope.entered = 1;
        mgs(cr).running = &mgs(cr).main;
        mgs(cr).running->scope = &g_mill_scope;

        mgs(dbg).all_crs.first = &mgs(cr).main.debug.item;
        mgs(dbg).all_crs.last = &mgs(cr).main.debug.item;

        mill_lock_init(&mgs(cr).woke_lock);

        mill_poller_enter(&mgs(poller));

        mgs(stack).boundary = 1 << (32 - __builtin_clz(
                (sizeof(struct mill_cr) + mgs(cr).valbuf_size) - 1));
        int rc = pipe(mgs(poker).fd);
        mill_assert(rc != -1);
        int opt = fcntl(mgs(poker).fd[0], F_GETFL, 0);
        if (opt == -1)
            opt = 0;
        rc = fcntl(mgs(poker).fd[0], F_SETFL, opt | O_NONBLOCK);
        mill_assert(rc != -1);
    }
}

inline int  mill_g_scope_count()
{
    return __sync_fetch_and_add(&s_tid, 0);
}

void mill_poller_poke(struct mill_scope* scope)
{
    mill_assert(scope->poker.fd[1] > 0);
    char x = 0;
    int rc = write(scope->poker.fd[1], &x, 1);
    mill_assert(rc == 1);
}