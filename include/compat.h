//
// Created by Mpho Mbotho on 2020-09-25.
//

#ifndef LIBMILL_COMPACT_H
#define LIBMILL_COMPACT_H

#ifndef __APPLE__
#include <sys/sendfile.h>
#define sendfile_compat sendfile
#else

#include <sys/types.h>
#include <sys/socket.h>
#include <sys/uio.h>

#define MSG_NOSIGNAL SO_NOSIGPIPE

ssize_t sendfile_compat(int out_fd, int in_fd, off_t *offset, size_t count);

#endif

#endif //LIBMILL_COMPACT_H
