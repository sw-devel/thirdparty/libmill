//
// Created by Mpho Mbotho on 2021-01-08.
//

#ifndef LIBMILL_MUTEX_H
#define LIBMILL_MUTEX_H

#include "scope.h"

#include <pthread.h>

struct mill_mutex {
    // A list of coroutines waiting for the lock
    struct mill_slist   waiting;
    // whether the lock is acquired or not
    int                 acquired;
    const char*         id;
    mill_lock_t         lk;
};

struct mill_event {
    // A list of coroutines waiting for the lock
    struct mill_list    waiting;
    // whether the lock is acquired or not
    int                 signaled;
    const char*         id;
    mill_lock_t         lk;
};

struct mill_mutex* mill_mutex_create_(int is, const char* current);
void mill_mutex_acquire_(struct mill_mutex*, const char* current);
void mill_mutex_release_(struct mill_mutex*);
int  mill_mutex_try_acquire_(struct mill_mutex*);
void mill_mutex_destroy_(struct mill_mutex*);

struct mill_event* mill_event_create_(const char* tag);
int  mill_event_wait_(struct mill_event* evt, int64_t dd, const char *current);
void mill_event_notify_(struct mill_event* evt);
void mill_event_notify_one_(struct mill_event* evt);
void mill_event_destroy_(struct mill_event* evt);


#endif //LIBMILL_MUTEX_H
