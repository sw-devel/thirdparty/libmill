//
// Created by Mpho Mbotho on 2021-01-09.
//

#ifndef INCLUDE_LIBMILL_HPP
#define INCLUDE_LIBMILL_HPP

#include <libmill/libmill.h>

namespace mill {

    template <typename F, typename...Args>
    MILL_EXPORT std::thread async_(F&& f, Args&&... args)
    {
        return std::thread([ff = std::forward<F>(f), ...fargs = std::forward<Args>(args)](){
            mill_scope_enter_();
            ff(std::forward<Args>(fargs)...);
        });
    }

    class Mutex {
    public:
        Mutex()
            : _mtx{mtxcreate(0)}
        {}
        Mutex(const Mutex&) = delete;
        Mutex& operator=(const Mutex&) = delete;
        Mutex(Mutex&& o)
            : _mtx{std::exchange(o._mtx, nullptr)}
        {}

        Mutex& operator=(Mutex&& o)
        {
            if (&o == this) {
                return *this;
            }
            _mtx = std::exchange(o._mtx, nullptr);
            return *this;
        }

        inline void acquire() {
            mtxacquire(_mtx);
        }

        inline bool tryAcquire() {
            return mtxtryacquire(_mtx) != 0;
        }

        inline void release() {
            mtxrelease(_mtx);
        }

        ~Mutex() {
            if (_mtx != nullptr) {
                mtxdestroy(_mtx);
                _mtx = nullptr;
            }
        }

    private:
        mmutex _mtx{nullptr};
    };

    class Lock {
    public:
        explicit  Lock(Mutex& mtx)
            : _mtx{mtx}
        {
            _mtx.acquire();
        }

        inline void lock() {
            _mtx.acquire();
        }

        inline void unlock() {
            _mtx.release();
        }

        ~Lock() {
            _mtx.release();
        }

    private:
        Lock(const Lock&) = delete;
        Lock(Lock&&) = delete;
        Lock& operator=(const Lock&) = delete;
        Lock& operator=(Lock&&) = delete;

        Mutex& _mtx;
    };

    class Event {
    public:
        Event()
            : _evt{evcreate()}
        {}

        Event(Event&& o)
            : _evt{std::exchange(o._evt, nullptr)}
        {}

        Event& operator=(Event&& o)
        {
            if (this == &o) {
                return *this;
            }
            _evt = std::exchange(o._evt, nullptr);
            return *this;
        }

        inline bool wait(Lock& lock, int64_t dd = -1) {
            lock.unlock();
            auto s = evwait(_evt, dd);
            lock.lock();
            return s == 0;
        }

        inline bool wait(int64_t dd = -1) {
            return evwait(_evt, dd) == 0;
        }

        inline void notify() {
            evnotify(_evt);
        }

        inline void notifyOne() {
            evnotifyone(_evt);
        }

        ~Event() {
            if (_evt != nullptr) {
                evdestroy(_evt);
                _evt = nullptr;
            }
        }

    private:
        Event(const Event&) = delete;
        Event& operator=(const Event&) = delete;
        mevent _evt{nullptr};
    };
}

#ifdef MILL_USE_PREFIX
#define mill_async libmill::async_
#else
#define masync mill::async_
#endif

#endif //INCLUDE_LIBMILL_HPP
