//
// Created by Mpho Mbotho on 2021-01-08.
//

#ifndef LIBMILL_SCOPE_H
#define LIBMILL_SCOPE_H

#include "cr.h"

#include <pthread.h>

#if defined __linux__ && !defined MILL_NO_EPOLL
#define MILL_EPOLL
#elif (defined BSD || defined __APPLE__) && !defined MILL_NO_KQUEUE
#define MILL_KQUEUE
#else
#define MILL_POLL
#endif
struct mill_poker {
    int fd[2];
    int poked;
};

#if defined(MILL_LIMIT_THREADS)
#define mill_lock_t pthread_spinlock_t
#define mill_lock_init(lk)    pthread_spin_init((lk), PTHREAD_PROCESS_PRIVATE)
#define mill_lock_acquire     pthread_spin_lock
#define mill_lock_release     pthread_spin_unlock
#define mill_lock_destroy     pthread_spin_destroy
#else
#define mill_lock_t pthread_mutex_t
#define mill_lock_init(lk)    pthread_mutex_init((lk), NULL)
#define mill_lock_acquire     pthread_mutex_lock
#define mill_lock_release     pthread_mutex_unlock
#define mill_lock_destroy     pthread_mutex_destroy
#endif

#ifdef MILL_EPOLL
struct mill_poller {
    int  efd;
    struct mill_crpair *crpairs;
    int ncrpairs;
    uint32_t changelist;
    int initialised;
};
#elif defined(MILL_KQUEUE)
struct mill_poller {
    int kfd;
    struct mill_crpair *crpairs;
    int ncrpairs;
    uint32_t changelist;
    int initialised;
};
#else
struct mill_poller {
    struct mill_pollset_item *pollset_items
    int mill_pollset_size;
    int mill_pollset_capacity;
    struct pollfd *mill_pollset_fds;
    int initialised;
};
#endif

struct mill_scope {

    struct {
        /**
         * Size of the buffer for temporary storage of values received from channels.
         * It should be properly aligned and never change if there are any stacks
         * allocated at the moment
         **/
        size_t valbuf_size;

        /**
         * Valbuf for tha main coroutine.
         * */
        char main_valbuf[128];

        struct mill_cr main;

        struct mill_cr *running;

        /**
         * Queue of coroutines scheduled for execution.
         **/
        struct mill_slist ready;

        /**
         * Queue of coroutines woken up from other threads
         **/
        struct mill_slist woke;
        /**
         * Protects the woke list
         */
        mill_lock_t woke_lock;
    } cr;

    struct {
        /**
         * ID to be assigned to next launched coroutine.
         **/
        int next_cr_id;

        /**
         * List of all coroutines.
         **/
        struct mill_list all_crs;

        /**
         * ID to be assigned to the next created channel.
         **/
        int next_chan_id;

        /**
         * List of all channels.
         **/
        struct mill_list all_chans;
    } dbg;

    struct mill_poller poller;

    struct {
        struct dns_resolv_conf *dns_conf;
        struct dns_hosts       *dns_hosts;
        struct dns_hints       *dns_hints ;
    } ip;

#if defined(SSL_SUPPORTED)
    struct {
        struct ssl_ctx_st *cli_ctx;
    } ssl;
#endif

    struct {
        /**
         * Stack size, as specified by the user.
         **/
        size_t stack_size;
        /**
         * Actual stack size.
         **/
        size_t sanitised_stack_size;

        /**
         * Maximum number of unused cached stacks. Keep in mind that we can't
         * deallocate the stack you are running on. Thus we need at least one cached
         * stack.
         **/
        int max_cached_stacks;

        /**
         * A stack of unused coroutine stacks. This allows for extra-fast allocation
         * of a new stack. The LIFO nature of this structure minimises cache misses.
         * When the stack is cached its mill_slist_item is placed on its top rather
         * then on the bottom. That way we minimise page misses.
         **/
        int num_cached_stacks;
        /**
         * A list of cached stacks
         */
        struct mill_slist cached_stacks;
        /**
         * This is the offset of the stack form the base of the memory allocated
         */
        int boundary;
    } stack;

    struct {
        int fd[2];
        int on;
        int waiting;
    } poker;

    /**
     * Global linked list of all timers. The list is ordered.
     * First timer to be resume comes first and so on.
     **/
    struct mill_list timers;

    int chan_choose_seqnum;

    int entered;

    int tid;
};

extern __thread struct mill_scope g_mill_scope;

void mill_scope_enter_();
int  mill_g_scope_count();
#define mgs(prop) g_mill_scope. prop

#endif //LIBMILL_SCOPE_H
